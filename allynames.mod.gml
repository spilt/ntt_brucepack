#define init
global.numerals = ["I","Jr.","III","IV","V","VI","VII","VIII","IX","X","XI","XII","XIII","XIV","XV","XVI","XVII","XVIII","XIX","XX"];
//trace("Allies now have names.");
global.allynames = ["Bob", "Weave", "Rami", "JW", "Joonas", "Jukio", "Tam", "Azite", "Stef",
 "Michael", "Jessica", "Christopher", "Ashley", "Matthew", "Emily", "Joshua", "Sarah", "Jacob", "Samantha",
 "Nicholas", "Amanda", "Andrew", "Brittany", "Daniel", "Elizabeth", "Tyler", "Taylor", "Joseph", "Megan", 
 "Brandon", "Hannah", "David", "Kayla", "James", "Lauren", "Ryan", "Stephanie", "John", "Rachel", 
 "Zachary", "Jennifer", "Justin", "Nicole", "William", "Alexis", "Anthony", "Victoria", "Robert", "Amber", 
 "Jonathan", "Alyssa", "Austin", "Courtney", "Alexander", "Rebecca", "Kyle", "Danielle", "Kevin", "Jasmine", 
 "Thomas", "Brianna", "Cody", "Katherine", "Jordan", "Alexandra", "Eric", "Madison", "Benjamin", "Morgan", 
 "Aaron", "Melissa", "Christian", "Michelle", "Samuel", "Kelsey", "Dylan", "Chelsea", "Steven", "Anna", 
 "Brian", "Kimberly", "Jose", "Tiffany", "Timothy", "Olivia", "Nathan", "Mary", "Adam", "Christina", 
 "Richard", "Allison", "Patrick", "Abigail", "Charles", "Sara", "Sean", "Shelby", "Jason", "Heather", 
 "Cameron", "Haley", "Jeremy", "Maria", "Mark", "Kaitlyn", "Stephen", "Laura", "Jesse", "Erin", "Juan", "Andrea", 
 "Alex", "Natalie", "Travis", "Jordan", "Jeffrey", "Brooke", "Ethan", "Julia", "Caleb", "Emma", "Luis", "Vanessa", 
 "Jared", "Erica", "Logan", "Sydney", "Hunter", "Kelly", "Trevor", "Kristen", "Bryan", "Katelyn", "Evan", "Marissa", 
 "Paul", "Amy", "Taylor", "Crystal", "Kenneth", "Paige", "Connor", "Cassandra", "Dustin", "Gabrielle", 
 "Noah", "Katie", "Carlos", "Caitlin", "Devin", "Lindsey", "Gabriel", "Destiny", "Ian", "Kathryn", 
 "Nathaniel", "Jacqueline", "Gregory", "Shannon", "Derek", "Jenna", "Corey", "Angela", "Jesus", "Savannah", 
 "Scott", "Mariah", "Bradley", "Alexandria", "Dakota", "Sierra", "Antonio", "Alicia", "Marcus", "Briana", 
 "Blake", "Miranda", "Garrett", "Jamie", "Edward", "Catherine", "Luke", "Brittney", "Shawn", "Breanna", 
 "Peter", "Grace", "Seth", "Monica", "Mitchell", "Sabrina", "Adrian", "Madeline", "Victor", "Caroline", 
 "Miguel", "Molly", "Shane", "Erika", "Chase", "Mackenzie", "Isaac", "Leah", "Spencer", "Diana", "Lucas", "Whitney", 
 "Jack", "Cheyenne", "Tanner", "Bailey", "Angel", "Christine", "Vincent", "Meghan", "Isaiah", "Lindsay", "Dalton", "Angelica", 
 "Brett", "Cynthia", "George", "Margaret", "Alejandro", "Kaitlin", "Elijah", "Alexa", "Cory", "Hailey", 
 "Cole", "Veronica", "Joel", "Melanie", "Erik", "Bianca", "Jake", "Autumn", "Mason", "Ariel", "Jorge", "Kristin", 
 "Dillon", "Bethany", "Raymond", "Lisa", "Colton", "Kristina", "Ricardo", "Holly", "Casey", "Leslie", "Francisco", "Casey", 
 "Brendan", "Chloe", "Devon", "April", "Keith", "Julie", "Colin", "Claire", "Wesley", "Kaylee", "Phillip", "Brenda", 
 "Oscar", "Kathleen", "Julian", "Rachael", "Johnathan", "Karen", "Eduardo", "Sophia", "Chad", "Patricia", 
 "Donald", "Gabriela", "Bryce", "Kendra", "Ronald", "Dominique", "Alec", "Kara", "Dominic", "Ana", "Grant", "Desiree", 
 "Martin", "Tara", "Henry", "Michaela", "Mario", "Brandi", "Xavier", "Carly", "Manuel", "Kylie", "Alan", "Karina", 
 "Derrick", "Adriana", "Frank", "Valerie", "Tristan", "Caitlyn", "Collin", "Natasha", "Omar", "Hayley", 
 "Jeremiah", "Rebekah", "Jackson", "Jocelyn", "Troy", "Cassidy", "Edgar", "Jade", "Javier", "Gabriella", 
 "Douglas", "Makayla", "Clayton", "Daisy", "Jonathon", "Jillian", "Nicolas", "Alison", "Andre", "Audrey", 
 "Maxwell", "Faith", "Ivan", "Angel", "Philip", "Nancy", "Levi", "Dana", "Sergio", "Krystal", "Roberto", "Alejandra", 
 "Darius", "Ariana", "Andres", "Summer", "Cristian", "Isabella", "Hector", "Mikayla", "Fernando", "Raven", 
 "Drew", "Katrina", "Curtis", "Kiara", "Gary", "Sandra", "Riley", "Meagan", "Johnny", "Lydia", "Max", "Kirsten", 
 "Dennis", "Chelsey", "Malik", "Zoe", "Wyatt", "Monique", "Cesar", "Claudia", "Edwin", "Mallory", "Gavin", "Joanna", 
 "Preston", "Deanna", "Marco", "Isabel", "Allen", "Ashlee", "Ruben", "Felicia", "Calvin", "Marisa", "Mathew", "Mercedes", 
 "Randy", "Mckenzie", "Brent", "Jasmin", "Jerry", "Krista", "Hayden", "Yesenia", "Alexis", "Diamond", "Brady", "Evelyn", 
 "Parker", "Cindy", "Tony", "Selena", "Pedro", "Brandy", "Craig", "Gina", "Larry", "Mia", "Bruce",
 "Archibald", "Gordo", "Spud", "George Foreman", "Lil' Bandit", "Pooter", "Scooter", "Axel", "Duffy", "Daddy",
 "Buzz", "Froggy", "Charlie", "Mac", "Dee", "Spike", "Jet", "Faye", "Ed", "Alphonse", "Urethra", "Mal", "Jayne", "Inara", "Wash",
 "Simon", "River", "Shep", "Derrial", "Georgia", "Winry", "Ripley", "Ash", "Jackie", "Bill", "Ted", "Elvis", "Roy", "Harry",
 "Ron", "Hermione", "Snake", "Frodo", "Samwise", "Merry", "Pip", "Tuco", "Angeleyes", "Blondie", "Conan", "Lana",
 "Arthur", "Ford", "Zaphod", "Aldo", "Joe", "Kiki", "Lawrence", "Leon", "Matilda", "Lupin", "Terry", "Graham",
 "Donnie", "Nick", "Dorian", "Tommy", "Lola", "Jean-Luc", "Geordi", "Beverly", "Truman", "Wallace", "Wayne", "Garth",
 "Toshiro", "Akira", "Tetsuo", "Sterling", "Malory", "Pam", "Cyril", "Ray", "Cheryl", "Woodhouse", "Gob", "George Michael",
 "Tobias", "Rock", "Carl", "Neil", "Abed", "Annie", "Jeff", "Britta", "Pierce", "Ben", "Chang", "Shirley", "Eustice",
 "Miriam", "Dirk", "Moss", "Russell", "Reinhardt", "Yang", "Yoko", "Maiza", "Miria", "Czeslaw", "Firo", "Ursula",
 "Che", "Vladimir", "Fidel", "Starchy", "Finn"];

global.adjectives = ["Reckless","Timid","Potato-aim","Shield-shooter","Bulletsponge","Bodyguard","Skillful","Badass",
    "Wimpy","Wussy","Ugly","One-eyed","Fearless","Brave","Bold","Lazy","Lively","Unkillable","Fat","Skinny","Tall","Short",
    "Buff","Tough","Big","Lil'","Speedy","Ol'","Prof.","Dr.","Handsome","Slow","Fast"];

global.viewer_names = undefined;
reset_ally_generations();
global.color_index = 1;

#define game_start
reset_ally_generations();
global.color_index = 1;

#define step
with Ally {
    if ("name" not in self) {
        if (is_undefined(global.viewer_names)) {
            var i = irandom(array_length_1d(global.allynames)-1);
            name = global.allynames[i];
        } else {
            var i = irandom(ds_list_size(global.viewer_names)-1);
            name = ds_list_find_value(global.viewer_names, i);
        }
        namealpha = 1;
        namecolor = make_color_hsv(255*((global.color_index*(1 + sqrt(5))/2) mod 1), 50, 255);
        global.color_index++;
        switch (name) {
            case "Tobias":
                image_blend = make_color_rgb(125,125,255);
                break;
        }
        if (random(1) < 1/20) {
            var adj = global.adjectives[irandom(array_length_1d(global.adjectives)-1)];
            switch (adj) {
                case "Unkillable":
                    my_health = ceil(my_health * 2);
                    unkillable = true;
                    break;
                case "Wussy":
                case "Wimpy":
                    my_health = ceil(my_health * .5);
                    break;
                case "Lively":
                case "Tough":
                    my_health = ceil(my_health * 1.5);
                    break;
                case "Lil'":
                    image_xscale *= 0.75;
                    image_yscale *= 0.75;
                    size *= .75;
                    my_health = ceil(my_health * .75);
                    break;
                case "Big":
                    image_xscale *= 1.5;
                    image_yscale *= 1.5;
                    size *= 1.5;
                    my_health = ceil(my_health * 1.25);
                    break;
                case "Fat":
                    image_xscale *= 1.25;
                    size *= 1.2;
                    my_health = ceil(my_health * 1.25);
                    break;
                case "Skinny":
                    image_xscale *= .75;
                    size *= .8;
                    my_health = ceil(my_health * .75);
                    break;
                case "Tall":
                    image_yscale *= 1.25;
                    size *= 1.2;
                    break;
                case "Short":
                    size *= .8;
                    image_yscale *= .75;
                    break;
                case "Dr.":
                    doctor = true;
                    break;
            }
            name = adj + " " + name;
        }
        var gen = global.generations[? name];
        if (is_undefined(gen)) {
            gen = 0;
        }
        global.generations[? name] = gen + 1;
        if (gen > 0) {
            var numeral = (gen < 20 ? global.numerals[gen] : string(gen));
            name += " " + numeral;
        }
    } else {
        namealpha -= 1/60;
        if (namealpha < 0) {namealpha = 0;}
        if ("unkillable" in self && my_health < 10) {
            my_health += 1;
        }
        if ("doctor" in self) {
            with HPPickup {
                var hp = self;
                if (place_meeting(x,y,other)) {
                    with Player {
                        if (race == "rebel") {
                            hp.x = x;
                            hp.y = y;
                        }
                    }
                }
            }
        }
        with Player {
            if (point_distance(other.x,other.y,mouse_x[index],mouse_y[index]) < 30) {
                other.namealpha = 1;
            }
        }
    }
}
with Player {
    var closest_ally;
    var closest_distance = 999999;
    with Ally {
        var dist = point_distance(x,y,mouse_x[other.index],mouse_y[other.index]);
        if (dist < closest_distance) {
            closest_ally = self;
            closest_distance = dist;
        }
    }
    if (closest_ally != undefined && closest_distance < 50) {
        closest_ally.namealpha = 1;
    }
}

#define draw
with Ally {
    if ("name" in self && namealpha > 0) {
        draw_set_halign(fa_center);
        draw_set_alpha(namealpha*.3);
        draw_set_color(c_black);
        draw_text(x+1,y-15,name);
        draw_text(x,y-15+1,name);
        draw_set_alpha(namealpha);
        draw_set_color(namecolor);
        draw_text(x,y-15,name);
        draw_set_alpha(1);
    }
}

#define chat_command(command, username)
if (command == "twitchallies") {
    if (username == "") {
        trace("Usage: /twitchallies <your channel name>");
        return true;
    }
    username = string_lower(username);
    if (!fork()) {
        return true;
    }
    if (file_loaded("twitch.json")) {
        file_unload("twitch.json");
    }
    trace("Loading Twitch viewers, this may hang the game for a bit...");
    file_download("https://tmi.twitch.tv/group/user/"+username+"/chatters", "twitch.json");
    while (!file_loaded("twitch.json")) wait 1;
    var response_text = string_load("twitch.json");
    var pos = string_pos('viewers', response_text)
    if (!pos) {
        return false;
    }
    pos += 12;
    var done = false;
    global.viewer_names = ds_list_create();
    var response_len = string_length(response_text);
    while (!done && pos < response_len) {
        switch(string_char_at(response_text, pos)) {
            case "]":
                done = true;
                break;
            case '"': {// Quote mark
                pos++;
                var namelen = 0;
                while (string_char_at(response_text, pos + namelen) != chr(34)) {
                    namelen++;
                }
                var viewer = string_copy(response_text, pos, namelen);
                ds_list_add(global.viewer_names, viewer);
                pos += namelen + 1;
                break;
            }
            case "":
                done = true;
                break;
            default:
                pos++;
                break;
        }
    }
    trace("Loaded " + string(ds_list_size(global.viewer_names)) + " viewer names.");
}

#define reset_ally_generations
global.generations = ds_map_create();
var first_gens = ["Tam", "Azite", "Stef", "Rami", "JW", "Joonas", "Jukio", "Paul", "Bruce"];
for (var f=0; f<array_length_1d(first_gens); f++) {
    global.generations[? first_gens[f]] = 1;
}
global.generations[? "Lupin"] = 2;

