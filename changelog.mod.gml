#define init
trace("Brucepack v1.5:
  + Improved to Fish/Robot/Chicken/Plant actives.
  + Made Rogue stronger against IDPD.
  + Gave Rebel allies names.
  + Gave +1 max HP to Meling.
  + Buffed Recycle Gland/Euphoria/Open Mind/Eagle Eyes/Last Wish.
  + Secondary weapon reloads at 25% speed in the background.
  + Improved grenades, blood hammer, toxic, and lightning.
  + Rebalanced Maggots, Flies, and Buff Gators.
  + Added /difficulty command to adjust difficulty setting.
  + Added level and difficulty to in-game UI.");