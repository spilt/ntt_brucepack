#define init
//trace("Robot and Chicken's active abilities now prefer weapons on the ground.");

#define step
with Player {
    var player = self;
    
    // Robot eats weapons on the ground first
    if (race == "robot") {
        if (button_pressed(index, "spec") && player.bwep != 0) {
            with WepPickup {
                if (place_meeting(x,y,player)) {
                    var playerwep = player.wep;
                    var playercurse = player.curse;
                    var playerwepflip = player.wepflip;
                    
                    var playerbwep = player.bwep;
                    var playerbcurse = player.bcurse;
                    var playerbwepflip = player.wepflip;
                    
                    var groundwep = wep;
                    var groundcurse = curse;
                    
                    // Set put A-wep into B-slot (so it gets ammo)
                    player.bwep = playerwep;
                    player.bcurse = playercurse;
                    player.bwepflip = playerwepflip;
                    
                    // Put B-wep on the ground
                    wep = playerbwep;
                    curse = playerbcurse;
                    ammo = 0;
                    silentpickup = true;
                    
                    // Put ground wep into A-slot so it gets eaten.
                    player.wep = groundwep;
                    player.curse = groundcurse;
                    player.wepflip = 1;
                }
            }
        }
    }
    
    // Chicken throws weapons on ground first
    if (race == "chicken") {
        // Swap thrown weapons with on-the-ground ones midair
        with ThrownWep {
            if ("groundchecked" not in self) {
                // Prevent this from happening after 1 frame
                groundchecked = true;
                
                with WepPickup {
                    if (place_meeting(x,y,player) && player.bwep == 0) {
                        var thrownwep = other.wep;
                        var throwncurse = other.curse;
                        
                        // Give ground weapon's attributes to thrown weapon
                        other.sprite_index = sprite_index;
                        other.wep = wep;
                        other.curse = curse;
                        
                        // Move player's A-weap into B-slot
                        player.bwep = player.wep;
                        player.bcurse = player.curse;
                        player.bwepflip = player.wepflip;
                        
                        // Put thrown weapon into player's A-slot
                        player.wep = thrownwep;
                        player.curse = throwncurse;
                        player.wepflip = 1;
                        
                        // Remove pickup
                        instance_destroy();
                    }
                }
            }
        }
    }
    
    // Robot and Chicken auto-pick up weapons until at capacity (excluding cursed weapons)
    if (race == "robot" || race == "chicken") {
        with WepPickup {
            if (place_meeting(x,y,player) && (("silentpickup" in self && silentpickup) || !curse)) {
                // Auto-pickup into A-slot
                if (player.wep == 0) {
                    // Shift A-wep into B-slot
                    player.bwep = player.wep;
                    player.bcurse = player.curse;
                    player.bwepflip = player.wepflip;
                    
                    // Put ground wep into A-slot
                    player.wep = wep;
                    player.curse = curse;
                    player.wepflip = 1;
                    if (ammo) {
                        var atyp = weapon_get_type(wep);
                        player.ammo[atyp] += 2*player.typ_ammo[atyp];
                        if (player.ammo[atyp] > player.typ_amax[atyp]) {
                            player.ammo[atyp] = player.typ_amax[atyp];
                        }
                    }
                    
                    if (!("silentpickup" in self && silentpickup)) {
                        sound_play(sndWeaponPickup);
                        var tip = instance_create(x,y,PopupText);
                        tip.mytext = string(weapon_get_name(wep))+"!";
                    }
                    if (!weapon_is_melee(wep)) {
                        player.gunangle = 0;
                    } else if (player.gunangle == 0) {
                        player.gunangle = choose(130,-130);
                    }
                    instance_destroy();
                // Auto-pickup into B-slot
                } else if (player.bwep == 0) {
                    // Put ground wep into B-slot
                    player.bwep = wep;
                    player.bcurse = curse;
                    player.bwepflip = 1;
                    
                    if (!("silentpickup" in self && silentpickup)) {
                        sound_play(sndWeaponPickup);
                        var tip = instance_create(x,y,PopupText);
                        tip.mytext = string("(" + weapon_get_name(wep))+"!)";
                        tip.image_blend = make_color_rgb(150,150,150);
                    }
                    if (ammo) {
                        var atyp = weapon_get_type(wep);
                        player.ammo[atyp] += 2*player.typ_ammo[atyp];
                        if (player.ammo[atyp] > player.typ_amax[atyp]) {
                            player.ammo[atyp] = player.typ_amax[atyp];
                        }
                    }
                    instance_destroy();
                }
            }
        }
    }
}