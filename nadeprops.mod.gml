#define init
//trace("Grenades break maggots and weak props, not vice versa.");
global.props = [Cactus, PlantPot, Bush, NightCactus, WaterPlant, MoneyPile, PizzaBox, Bones];
global.propkillers = [Grenade, Rocket, Nuke, ToxicGrenade, HeavyNade, BloodGrenade, ClusterNade, PopoRocket];

#define step
for (var i=0; i < array_length_1d(global.propkillers); i++) with global.propkillers[i] {
    for (var p=0; p < array_length_1d(global.props); p++) with global.props[p] {
        if (point_distance(x,y,other.x,other.y) < 30) {
            instance_destroy();
        }
    }
    var maggots = [Maggot, RadMaggot];
    for (var m=0; m<2; m++) with maggots[m] {
        if (team != other.team && point_distance(x,y,other.x,other.y) < 30) {
            if (my_health <= 2) {
                my_health = 0;
            }
        }
    }
}