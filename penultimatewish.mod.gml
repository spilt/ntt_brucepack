#define init
//trace("Last Wish now has a 10% chance of procing every portal.");

#define step
with Player {
    var player = self;
    
    // Last Wish sometimes replentishes health
    if (skill_get(18)) {
        var portals = [Portal, BigPortal];
        for (var i = 0; i < array_length_1d(portals); i++) {
            with portals[i] {
                if ("lastwished" not in self && place_meeting(x,y,player)) {
                    lastwished = true;
                    if (random(1) < .1) {
                        player.curse = 0;
                        player.bcurse = 0;
                        player.my_health = player.maxhealth;
                        if (player.chickendeaths > 0) {
                            player.chickendeaths -= 1;
                        }
                        player.typ_ammo[1] += 180; // bullet
                        for (var ammo=1; ammo <= 5; ammo++) {
                            player.typ_ammo[ammo] += 20;
                            if (player.typ_ammo[ammo] > player.amax[ammo]) {
                                player.typ_ammo[ammo] = player.amax[ammo];
                            }
                        }
                        // TODO: detect Super Portal Strike?
                        if (player.race == "rogue" && player.rogueammo < 3) {
                            player.rogueammo = 3;
                        }
                        sound_play(sndMutLastWish);
                    }
                }
            }
        }
    }
}