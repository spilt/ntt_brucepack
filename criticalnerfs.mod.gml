#define init
//trace("Maggots now squish underfoot.");

#define step
with Player {
    var player = self;
    // Kill maggots on contact
    with Maggot {
        if (place_meeting(x,y,player)) {
            my_health -= 1;
        }
    }
}
with JungleFly {
    if ("nerfed" not in self) {
        nerfed = true;
        my_health = ceil(my_health * 0.75);
    }
}
with EFlakBullet {
    if ("nerfed" not in self) {
        nerfed = true;
        damage = floor(damage * 0.7);
    }
}