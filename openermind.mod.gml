#define init
//trace("Open mind now has a 1% chance to convert weapon chests to YV chests.");
global.better_weapon_buff = 0;

#define game_start
global.better_weapon_buff = 0;
/* For debugging*/
//skill_set(28,1);


#define step
var anyone_open_mind = false;
with Player {
    if (skill_get(28)) {
        anyone_open_mind = true;
        with WeaponChest {
            if ("openminded" not in self) {
                openminded = true;
                // For debugging, change .05 to 1.0
                if (random(1) < .05) {
                    instance_change(GiantWeaponChest, false);
                    instance_create(x,y,PortalClear);
                }
            }
        }
        // No mimics
        with Mimic {
            if ("safe_from_open_mind" not in self) {
                if (random(1) < 0.5) {
                    instance_change(AmmoChest, false);
                    image_speed = 0;
                } else {
                    safe_from_open_mind = true;
                }
            }
        }
        with SuperMimic {
            if ("safe_from_open_mind" not in self) {
                if (random(1) < 0.5) {
                    instance_change(HealthChest, false);
                    image_speed = 0;
                } else {
                    safe_from_open_mind = true;
                }
            }
        }
    }
}
if (anyone_open_mind && instance_exists(GenCont) && global.better_weapon_buff != 0) {
    GameCont.hard -= global.better_weapon_buff;
    global.better_weapon_buff = 0;
} else if (anyone_open_mind && !instance_exists(GenCont) && global.better_weapon_buff == 0) {
    global.better_weapon_buff = 1;
    GameCont.hard += global.better_weapon_buff;
}