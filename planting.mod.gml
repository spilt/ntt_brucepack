#define step
with TangleSeed {
    if (speed > 10) {
        speed = 10;
    }
    var seed = self;
    var hit_proj = false;
    with projectile {
        if (!hit_proj && team != seed.creator.team && place_meeting(x,y,seed)) {
            instance_destroy();
            hit_proj = true;
        }
    }
    if (hit_proj) {
        instance_destroy();
        sound_play(sndPlantSnare);
    }
}

with Player {
    var player = self;
    if (race == "plant") {
        if (button_pressed(index, "spec")) {
            var ntangles = 1;
            // Trapper
            if (ultra_get(5,1)) { ntangles = 5; }
            with Tangle {
                if (creator == player) {
                    instance_destroy();
                }
            }
            with TangleSeed {
                if ("creator" in self && creator == player) {
                    var _x = x;
                    var _y = y;
                    var _p = p;
                    var _typ = typ;
                    var _bskin = bskin;
                    wait 1;
                    with TangleSeed {
                        if (creator == player) {
                            instance_destroy();
                        }
                    }
                    for (var i=0; i < ntangles; i++) {
                        var _angle = random(0.5*6.28/(ntangles-1));
                        var tx = i == 0? _x : _x + 35*cos(_angle + 6.28*(i-1)/(ntangles-1));
                        var ty = i == 0? _y : _y + 35*sin(_angle + 6.28*(i-1)/(ntangles-1));
                        var new_tangle = instance_create(tx,ty,Tangle);
                        new_tangle.creator = player;
                        new_tangle.typ = _typ;
                        new_tangle.bskin = _bskin;
                        new_tangle.p = _p;
                    }
                }
            }
        }
    }
}

with Tangle {
    var tangle = self;
    var pickups = [HPPickup, AmmoPickup, Rad, BigRad];
    for(var i=0; i<array_length_1d(pickups); i++) with pickups[i] {
        if (place_meeting(x,y,tangle) && "uprooted" not in self) {
            uprooted = true;
            instance_create(x,y,RobotEat);
            x = tangle.creator.x;
            y = tangle.creator.y;
            sound_play(sndRobotEat);
        }
    }
}