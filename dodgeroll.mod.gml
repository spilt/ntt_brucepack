#define init
//trace("Fish can dodge roll!");

#define step
with Player {
    var player = self;
    // Fish can dodge roll!
    if (race == "fish") {
        if ("rollframe" not in player || "dizzy" not in player) {
            rollframe = 0;
            dizzy = 0;
        }
        // Tint darker while iframes apply
        if (rollframe > 1 && rollframe < 10) {
            image_blend = make_color_rgb(150,150,150);
        } else {
            image_blend = c_white;
        }
        // Kinda hacky. Ideally player would do nothing when colliding. Instead I temporarily swap team allegiance
        with projectile {
            if (other.roll && other.rollframe > 1 && other.rollframe < 10
                && distance_to_object(other) < 11 && team != other.team) {
                normal_team = team;
                team = other.team;
                instance_create(x,y,Deflect);
                sound_play(sndCrystalShield);
            } else if (distance_to_object(other) >= 11 && "normal_team" in self) {
                team = normal_team;
                normal_team = undefined;
            }
        }
        // Rolling makes you dizzy
        if (roll) {
            rollframe += 1;
            // dizzy growth
            dizzy = .995*dizzy + .005*1;
        } else {
            rollframe = 0;
            // dizzy decay
            dizzy = .995*dizzy + .005*0;
        }
        
        // Dizzy makes you vomit
        if (dizzy > 0.4 && random(1) < 0.01) {
            var puke = instance_create(x,y,CustomObject);
            puke.sprite_index = sprScorchmark;
            puke.image_blend = make_color_rgb(100,255,100);
            puke.image_speed = 0;
            puke.image_index = irandom(puke.image_number);
            puke.speed = 4;
            puke.friction = 0.5;
            puke.direction = random(360);
            puke.depth = 2;
            sound_play(sndRatKingVomit);
            dizzy -= 0.05;
        }
        // Throne butt re-rolls
        if (rollframe >= 12 && skill_get(5)) {
            sound_play(sndFishRollUpg);
            rollframe = 0;
        }
        // Being dizzy makes your accuracy go potato (reduced if you have Eagle Eyes)
        if (!weapon_is_melee(wep)) {
            accuracy = skill_get(19) ? .25 + 5*dizzy*dizzy : 1 + 10*dizzy*dizzy;
        }
        
        // Being dizzy makes you unsteady on your feet (unless you have Extra Feet)
        if (dizzy > .1 && !skill_get(2)) {
            // http://www.gmlscripts.com/script/gauss
            var x1, x2, w;
            do {
                x1 = random(2) - 1;
                x2 = random(2) - 1;
                w = x1*x1 + x2*x2;
            } until (0 < w && w < 1);

            w = sqrt(-2 * ln(w)/w);
            
            direction += x1*w*30*dizzy*(skill_get(19) ? .5 : 1);
            
            // VFX
            if (random(1) < dizzy*dizzy) {
                var fx = instance_create(x - (right ? -1 : 1) * (2 + random(6)),y - 3 + random(5), DustOLD);
                fx.depth = -3;
                fx.vspeed = player.vspeed - 1 - random(2);
                fx.hspeed = player.hspeed - (right ? -1 : 1) * (2 + random(2));
                fx.friction = 0.75;
                fx.gravity = 0.5;
                fx.gravity_direction = 90;
            }
        }
    }
}