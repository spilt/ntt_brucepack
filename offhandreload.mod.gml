#define init
//trace("Secondary weapon gets passively reloaded at 25% speed.");

#define step
with Player {
    // Auto-reload off-hand weapon at 25% speed
    if (race != "steroids") {
        breload -= 0.25;
        if (breload < 0) {
            breload = 0;
        }
    }
}