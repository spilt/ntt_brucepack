#define init
//trace("Rogue is now 25% stronger against IDPD.");
global.idpd = [Van, PopoFreak, Grunt, EliteGrunt, Shielder, EliteShielder, Inspector, EliteInspector, LilHunter, Last];
global.idpd_projectiles = [PopoExplosion, PopoSlug, PopoPlasmaBall, PopoRocket, PopoNade, IDPDBullet, LastBall, LastFire];

#define step
with Player {
    var player = self;
    
    // Rogue is strong against IDPD
    if (race == "rogue") {
        // IDPD have 25% less health
        for(var i = 0; i < array_length_1d(global.idpd); i++) {
            with global.idpd[i] {
                if ("roguenerfed" not in self) {
                    roguenerfed = true;
                    my_health = ceil(my_health * 0.75);
                }
            }
        }
        // IDPD projectiles do 50% less damage
        for(var i = 0; i < array_length_1d(global.idpd_projectiles); i++) {
            with global.idpd_projectiles[i] {
                if (team != player.team && "roguenerfed" not in self) {
                    roguenerfed = true;
                    damage = ceil(damage * 0.5);
                }
            }
        }
    }
}