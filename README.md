# Brucepack

The brucepack is a set of mods for [Nuclear Throne Together](https://yellowafterlife.itch.io/nuclear-throne-together).
The mods are mostly balancing and quality-of-life improvements.

## Instructions

1. Install the Nuclear Throne Together mod from: https://yellowafterlife.itch.io/nuclear-throne-together
2. Move the "brucepack" directory to the Nuclear Throne mods folder.
3. Launch the game and press "t" to open a command prompt.
4. Type "/loadtext brucepack" and hit enter.
5. Happy throning :)

## Mod Changes

### Difficulty

Because most of the other changes make the game easier, I've increased the overall
game difficulty by speeding up enemy AI and all projectiles. This makes the game
faster paced and a little more chaotic. Hopefully this should balance out to roughly
the same difficulty as vanilla Nuclear Throne.

Also, there is now a /difficulty command:
  + "/difficulty noob" disables the AI/projectile speed changes.
  + "/difficulty normal" (default setting) enables the AI/projectile speed changes.
  + "/difficulty hard/nightmare/infinite" same thing, but more so.

### Character Changes
  + **Fish** can dodge roll! (and get dizzy)
    - ![dodge roll](https://bitbucket.org/spilt/ntt_brucepack/downloads/dodgeroll.gif)
  + **Robot** eats weapons on the ground before weapons in hand.
    - ![robot eating](https://bitbucket.org/spilt/ntt_brucepack/downloads/groundweapons.gif)
  + **Chicken** throws weapons on the ground before weapons in hand.
  + **Rogue** is stronger against IDPD.
    - IDPD have 25% less health.
    - IDPD projectiles do 50% less damage.
  + **Rebel** allies have names.
    - You can use `/twitchallies <your twitch channel name>` to load viewer names as ally names.
    - ![ally names](https://bitbucket.org/spilt/ntt_brucepack/downloads/allynames.gif)
  + **Plant**'s active is better:
    - Plant picks up drops that land on the snare.
    - Snare can be activated midair by right clicking.
    - Seed projectile speed is slower to facilitate placement.
    - If the seed hits a projectile, both get deleted.
  + **Melting** starts with 3 HP instead of 2 HP.

### Mutation Changes
  + **Recycle Gland** absorbs reflected bullets.
  + **Euphoria** will trigger the following when you are close to enemy bullets:
    - The game slows down.
    - Bullets have rainbow "bullet time" trails.
    - Reload speed increases.
  + **Open Mind** buffed:
    - Weapon drops are 1 level better.
    - Weapon chests have a 5% chance of becoming YV chests.
    - 50% fewer mimic spawns.
  + **Eagle Eyes** buffed:
    - Makes you see further.
    - Reveals mimics and assassins.
    - Shows map indicators for chests, bosses, and special items.
    - Gives better night vision.
    - No longer ruins melee weapons like the Shovel.
    - ![eaglier eyes](https://bitbucket.org/spilt/ntt_brucepack/downloads/eagliereyes.gif)
  + **Last Wish** has 10% chance of activating on every portal.

### Weapon Changes
  + Lightning forks.
  + Secondary weapon reloads at 25% speed in the background.
  + Grenades destroy maggots and weak props without detonating.
  + Toxic gas does no damage until it fills a meter.
    - ![gas](https://bitbucket.org/spilt/ntt_brucepack/downloads/toxicity.gif)
  + Blood hammer only harms player if player is at full health and misses enemies.

### Enemy Changes
  + Maggots squish underfoot.
  + Flies have 25% less HP.
  + Buff Gator flak does 30% less damage.

### Misc Changes
  + HUD draws level, loop number, and difficulty setting.