#define init
global.trippiness = 0;

#define game_start
// Debug purposes
//skill_set(12,1);

#define draw
var has_euphoria = false;
with Player {
    if (skill_get(12)) {
        has_euphoria = true;
    }
}
if (has_euphoria && instance_exists(Player)) {
    global.trippiness += 1;
    var t = global.trippiness/60;
    var max_intensity = 0;
    var euphoric_total = 0;
    var color = make_color_hsv(255*((t*6) mod 1), 255, 255);
    with projectile {
        var player = instance_nearest(x,y,Player);
        var dist = point_distance(x,y,player.x,player.y);
        var _intensity = (100-dist)/100;
        if (_intensity <= 0) { continue; }
        _intensity *= _intensity;
        if (speed == 0 || team == player.team) { continue; }
        
        var ignore = [Slash,GuitarSlash,BloodSlash,EnergySlash,Shank,EnergyShank,
            EnergyHammerSlash,LightningSlash,EnemySlash,NothingBeam];
        var ignored = false;
        for (var ig=0; ig<array_length_1d(ignore) && !ignored; ig++) {
            if (object_index == ignore[ig]) {
                ignored = true;
            }
        }
        if (ignored) { continue; }

        
        _intensity *= 1-power(0.8, damage);
        if (_intensity <= 0) { continue; }
        euphoric_total += _intensity;
        if (_intensity > max_intensity) {
            max_intensity = _intensity;
        }
        if (_intensity < 0.01) { continue; }
        var _xscale = ("right" in self)? (right? 1 : -1) : image_xscale;
        
        // Draw bullet time trails
        var too_much_drawing = [NothingBeam,HorrorBullet,TrapFire,EnemyBullet4,EliteGruntFlame];
        var skip_trail = false;
        for (var ig=0; ig<array_length_1d(too_much_drawing) && !skip_trail; ig++) {
            if (object_index == too_much_drawing[ig]) {
                ignored = skip_trail;
            }
        }
        if (!skip_trail) {
            if ("prevarray" not in self) {
                prevarray = [{x:x,y:y},{x:x,y:y},{x:x,y:y},{x:x,y:y},{x:x,y:y},{x:x,y:y},{x:x,y:y},{x:x,y:y},{x:x,y:y},{x:x,y:y},{x:x,y:y}];
                prevoffset = 0;
            } else {
                for (var p_i=1; p_i<array_length_1d(prevarray); p_i++) {
                    prevarray[p_i-1] = prevarray[p_i];
                }
                prevarray[array_length_1d(prevarray)-1] = {x:x,y:y};
                prevoffset = (prevoffset + 3) mod 4;
            }
            var nprev = array_length_1d(prevarray);
            for (var p_i=prevoffset; p_i<nprev; p_i+=4) {
                var _x = prevarray[p_i].x;
                var _y = prevarray[p_i].y;
                var k = (p_i+1)/nprev;
                var size = 1+1.0*(1-k*k);
                draw_sprite_ext(sprite_index, image_index, _x, _y, _xscale*size, image_yscale*size, image_angle, c_white, _intensity*k);
            }
        }
        
        // Draw rainbow effects
        d3d_set_fog(1, color, 0, 1);
        draw_set_blend_mode(bm_add);
        var _passes = 6;
        var _x = x;
        var _y = y;
        for(var i = 0; i < 360; i += 360/_passes)
        {
            draw_sprite_ext(sprite_index, image_index, _x+lengthdir_x(3, i+t*2), _y+lengthdir_y(3, i+t*2), _xscale, image_yscale, image_angle, color, _intensity*1/_passes);
        }
        draw_sprite_ext(sprite_index, image_index, _x, _y, _xscale, image_yscale, image_angle, color, .5+.5*_intensity);
        draw_set_blend_mode(bm_normal);
        d3d_set_fog(0, 0, 0, 0);
        draw_set_color(c_white);
    }
    
    // Slow time and increase reload
    var euphoric_frames = floor(30*(1-power(0.6, euphoric_total)) + random(1));
    if (euphoric_frames > 0) {
        sleep(euphoric_frames);
        with Player {
            // Continue reloading during lag
            reload -= euphoric_frames*5/30;
            if (reload < 0) {
                reload = 0;
            }
        }
    }
}