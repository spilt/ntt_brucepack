#define init
//trace("Toxicity is not instant.");
global.MAX_TOXIC_IFRAMES = 30;

#define step
with Player {
    if (race != "frog") {
        if ("toxic_iframes" not in self) {
            toxic_iframes = global.MAX_TOXIC_IFRAMES;
        }
        if (toxic_iframes > 0) {
            var toxicnow = false;
            with ToxicGas {
                if (point_distance(x,y,other.x,other.y) < 25) {
                    team = other.team;
                    toxicnow = true;
                }
            }
            if (toxicnow) {
                toxic_iframes -= 1;
                if (toxic_iframes < 0) toxic_iframes = 0;
            } else {
                toxic_iframes += 0.5;
                if (toxic_iframes > global.MAX_TOXIC_IFRAMES)
                    toxic_iframes = global.MAX_TOXIC_IFRAMES;
            }
        } else {
            with ToxicGas {
                team = 0;
            }
            toxic_iframes += 0.5;
        }
    }
}

#define draw
with Player {
    if ("toxic_iframes" in self && toxic_iframes < global.MAX_TOXIC_IFRAMES) {
        var t = (1-toxic_iframes/global.MAX_TOXIC_IFRAMES);
        var w = 20*t;
        draw_set_alpha(t < .2 ? t/.2 : 1);
        draw_set_color(make_color_rgb(0,50,0));
        draw_rectangle(x-11, y-18, x+11, y-13, false);
        draw_set_color(make_color_rgb(50,250,0));
        draw_rectangle(x-10, y-17, x-10+w, y-14, false);
    }
}