#define init
global.blink_frame = 0;

/* for debugging:*/
#define game_start
//skill_set(19,1);

#define step
with Player {
    if (skill_get(19)) {
        if (weapon_is_melee(wep)) {
            accuracy = 1;
        }
    }
}

#define draw
var has_eagleeyes = false;
with Player {
    has_eagleeyes |= skill_get(19);
}
global.blink_frame += 1;
if (has_eagleeyes) {
    var alarming = [Mimic,SuperMimic,MeleeFake,JungleAssassinHide];
    for(var a=0; a<array_length_1d(alarming);a++) with alarming[a] {
        var _intensity = 0.15 + 0.07*sin(global.blink_frame/60 * 6.28 * 5);
        d3d_set_fog(1, c_red, 0, 1);
		draw_set_blend_mode(bm_add);
		var _passes = 8;
		for(var i = 0; i < 360; i += 360/_passes)
		{
			draw_sprite_ext(sprite_index, image_index, x+lengthdir_x(3, i+global.blink_frame*2), y+lengthdir_y(3, i+global.blink_frame*2), image_xscale, image_yscale, image_angle, c_red, _intensity/4);
		}
		draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, c_red, _intensity);
		draw_set_blend_mode(bm_normal);
		d3d_set_fog(0, 0, 0, 0);
        draw_set_color(c_white);
    }
}

#define draw_gui
with Player {
    if (!instance_exists(GenCont) && skill_get(19)) {
        var vx = view_xview[index];
        var vy = view_yview[index];
        view_pan_factor[index] = weapon_get_type(wep) == 3? 2.0 : 2.25;
        //trace(view_pan_factor[index]);
        var vh = 240;
        var vw = 320;
        var dotted = [
            // Ammo
            {obj:AmmoChest, color:make_color_rgb(255,200,0), radius:1},
            {obj:AmmoChestMystery, color:make_color_rgb(255,200,0), radius:1},
            {obj:GiantAmmoChest, color:make_color_rgb(255,200,0), radius:1},
            {obj:IDPDChest, color:make_color_rgb(255,200,0), radius:1},
            // Weps
            {obj:WeaponChest, color:make_color_rgb(200,0,0), radius:1},
            {obj:BigWeaponChest, color:make_color_rgb(200,0,0), radius:2},
            {obj:BigCursedChest, color:make_color_rgb(200,0,0), radius:1},
            {obj:GoldChest, color:make_color_rgb(255,220,100), radius:1},
            {obj:GiantWeaponChest, color:make_color_rgb(255,220,100), radius:1},
            // Rads
            {obj:RadChest, color:make_color_rgb(150,255,90), radius:1},
            {obj:RadMaggotChest, color:make_color_rgb(150,255,90), radius:1},
            // Misc
            {obj:RogueChest, color:make_color_rgb(100,100,255), radius:1},
            {obj:HealthChest, color:c_white, radius:1},
            {obj:Van, color:make_color_rgb(70,70,255), radius:2},
            {obj:IceFlower, color:make_color_rgb(255,200,180), radius:2},
            // Bosses
            {obj:BanditBoss, color:make_color_rgb(175,175,150), radius:2},
            {obj:ScrapBoss, color:make_color_rgb(120,120,150), radius:2},
            {obj:LilHunter, color:make_color_rgb(0,0,255), radius:2},
            {obj:LilHunterDie, color:make_color_rgb(0,0,255), radius:2},
            {obj:Nothing, color:make_color_rgb(100,0,0), radius:2},
            {obj:Nothing2, color:make_color_rgb(0,100,0), radius:2},
            {obj:FrogQueen, color:make_color_rgb(100,0,200), radius:2},
            {obj:HyperCrystal, color:make_color_rgb(255,150,0), radius:2},
            {obj:TechnoMancer, color:make_color_rgb(75,75,100), radius:2},
            {obj:Last, color:make_color_rgb(0,0,200), radius:2},
            {obj:BigFish, color:make_color_rgb(75,0,200), radius:2},
            {obj:EnemyHorror, color:make_color_rgb(150,255,90), radius:2}
        ];
        for (var d=0; d<array_length_1d(dotted); d++) with dotted[d].obj {
            if (!(vx <= x && x <= vx+vw && vy <= y && y <= vy+vh)) {
                var px;
                var py;
                var r = dotted[d].radius;
                var walls = [];
                walls[0] = [{x:(r+0),y:(r+2)}, {x:vw-(r+2),y:(r+2)}];
                walls[1] = [{x:(r+0),y:(r+2)}, {x:(r+0),y:vh-(r+0)}];
                walls[2] = [{x:(r+0),y:vh-(r+0)}, {x:vw-(r+2),y:vh-(r+0)}];
                walls[3] = [{x:vw-(r+2),y:(r+2)}, {x:vw-(r+2),y:vh-(r+0)}];
                var rayOrigin = {x:vw/2, y:vh/2};
                var _angle = degtorad(point_direction(rayOrigin.x,rayOrigin.y,x-vx,y-vy));
                var rayDirection = {x:cos(_angle), y:sin(_angle)};
                var intersection = 0;
                for (var w=0; w<4; w++) {
                    if (intersection == 0) {
                        intersection = point_intersect(rayOrigin,rayDirection,walls[w][0],walls[w][1]);
                    }
                }
                if (intersection != 0) {
                    var px = floor(intersection.x + .5);
                    var py = floor(vh-intersection.y + .5);
                    draw_set_alpha(0.5);
                    draw_set_color(c_black);
                    draw_circle(px,py,r+1,false);
                    draw_set_alpha(1);
                    draw_set_color(dotted[d].color);
                    draw_circle(px,py,r,false);
                }
            }
        }
    }
}

#define draw_dark
with TopCont {
    if (darkness == 1) {
        with Player {
            if (skill_get(19)) {
                if (race == "eyes") {
                    for (var i=0; i < 9; i++) {
                        draw_eyebeam(x,y,360*i/9-90);
                    }
                } else {
                    var mouse_dir = -point_direction(x,y,mouse_x[index],mouse_y[index]);
                    draw_eyebeam(x,y,mouse_dir);
                }
            }
        }
    }
}


#define draw_eyebeam(x,y,angle)
draw_set_alpha(1);
draw_set_color($808080);
var w1 = 16 + random(1);
draw_triangle(x,y,
    x+1000*cos(degtorad(angle-w1/2)),y+1000*sin(degtorad(angle-w1/2)),
    x+1000*cos(degtorad(angle+w1/2)),y+1000*sin(degtorad(angle+w1/2)),
    false);
draw_set_color(0);
var w2 = 12 + random(1);
draw_triangle(x,y,
    x+1000*cos(degtorad(angle-w2/2)),y+1000*sin(degtorad(angle-w2/2)),
    x+1000*cos(degtorad(angle+w2/2)),y+1000*sin(degtorad(angle+w2/2)),
    false);


#define point_intersect(rayOrigin, rayDirection, point1, point2)
var v1 = {x:rayOrigin.x - point1.x, y:rayOrigin.y - point1.y};
var v2 = {x:point2.x - point1.x, y:point2.y - point1.y};
var v3 = {x:-rayDirection.y, y:rayDirection.x};

var dot = v2.x*v3.x + v2.y*v3.y;
if (abs(dot) < 0.000001)
    return 0;

var t1 = (v2.x*v1.y - v2.y*v1.x) / dot;
var t2 = (v1.x*v3.x + v1.y*v3.y) / dot;

if (t1 >= 0.0 && t2 >= 0.0 && t2 <= 1.0)
    return {x:rayOrigin.x + t1*rayDirection.x, y:rayOrigin.y + t1*rayDirection.y};

return 0;