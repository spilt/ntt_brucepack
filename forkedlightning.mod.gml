#define init
//trace("Lighning now forks.");

#define step
// Make lightning useful
with Lightning {
    if ("forked" not in self) {
        forked = true;
        sound_play(sndLightningPistol);
        if (instance_number(Lightning) < 100 && ammo > 3 && random(1) < 0.1) {
            with (instance_create(x, y, Lightning)) {
                ammo = other.ammo - 3 + irandom(3);
                team = other.team;
                alarm0 = 1;
                image_angle = other.image_angle - 45 + random(90);
                with (instance_create(x, y, LightningSpawn)) {
                    image_angle = other.image_angle;
                }
            }
        }
    }
}