#define init
global.bruce_difficulty = 1;
global.ai_clock = 0;

#define game_start
global.ai_clock = 0;

#define step
global.ai_clock += 1;
var thinkbuff = [999, 4, 3, 2, 2];
if ((global.ai_clock mod (thinkbuff[global.bruce_difficulty])) == 0) {
    with enemy {
        if (alarm1 > 1) {
            alarm1 -= 1;
        }
    }
}

var speedbuff = [1, 1.3, 1.6, 2.5, 3];
with projectile {
    if ("buffed" not in self) {
        buffed = true;
        if (team != 2 && friction == 0 && object_index != TrapFire && object_index != BouncerBullet) {
            if (object_index == LHBouncer) {
                speed *= .5*speedbuff[global.bruce_difficulty] + .5*1.0;
            } else {
                speed *= speedbuff[global.bruce_difficulty];
            }
        }
    }
}

if (global.bruce_difficulty == 4) {
    with Player {
        my_health = 0;
    }
}

#define chat_command(command, level)
if (command == "difficulty") {
    if (level == "noob" || level == "n00b" || level == "2yung2die" || level == "0") {
        global.bruce_difficulty = 0;
        //trace("Difficulty set to 'noob'");
        trace("First time playing, eh?");
    } else if (level == "normal" || level == "hurtmeplenty" || level == "1") {
        global.bruce_difficulty = 1;
        //trace("Difficulty set to 'normal'");
        trace("Set to normal difficulty.");
    } else if (level == "hard" || level == "ultra-violence" || level == "2") {
        global.bruce_difficulty = 2;
        //trace("Difficulty set to 'hard'");
        trace("As JW intended it.");
    } else if (level == "nightmare" || level == "3") {
        global.bruce_difficulty = 3;
        //trace("Difficulty set to 'nightmare'");
        trace("oh god why");
    } else if (level == "infinite" || level == "4") {
        global.bruce_difficulty = 4;
        trace("You asked for it.");
    } else {
        trace("Not a valid option. Options are: noob, normal, hard, nightmare, infinite");
    }
    return true;
}

#define draw_gui
if (instance_exists(Player)) {
    var vw = 320;
    var vh = 240;
    var s = "";
    s += (GameCont.loops > 0? "L"+string(GameCont.loops)+" " : "   ");
    if (GameCont.area == 100) {
        s += "???";
    } else {
        s += string(GameCont.area) + "-" + string(GameCont.subarea);
    }
    var difficulty_names = [" n00b",""," hard"," nmare"," inf"];
    s += difficulty_names[global.bruce_difficulty];
    var x = vw-3;
    var y = 23;
    draw_set_halign(fa_right);
    draw_set_alpha(.1);
    draw_set_color(c_black);
    draw_text(x+1,y,s);
    draw_text(x,y+1,s);
    draw_set_alpha(.5);
    draw_set_color(c_white);
    draw_text(x,y,s);
    draw_set_alpha(1);
    draw_set_halign(fa_left);
}