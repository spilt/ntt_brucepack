#define init
//trace("Reflected bullets now get absorbed by recycle gland.");
global.recyclables = [Bullet1, AllyBullet, BouncerBullet, UltraBullet, HeavyBullet,
    Burst, GoldBurst, HeavyBurst, HyperBurst, RogueBurst, Bullet2, PopBurst];

#define step
// Recycle gland absorbs reflected bullets
with Player {
    var player = self;
    if (skill_get(16)) {
        for (var i=0; i < array_length_1d(global.recyclables); i++) {
            with global.recyclables[i] {
                if (point_distance(player.x,player.y,x,y) <= 30 && team != player.team) {
                    instance_create(x,y,RecycleGland);
                    sound_play(sndMutRecycleGland);
                    instance_destroy();
                }
            }
        }
    }
}